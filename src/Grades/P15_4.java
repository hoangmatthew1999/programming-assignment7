package Grades;
import java.util.HashMap;
import java.util.Scanner;
public class P15_4 {
    HashMap<String,String> gradesDB = new HashMap<String,String>();
    public void addStudent(){
        Scanner scanner = new Scanner(System.in);

        String nameInput;
        System.out.println("Enter student name");
        nameInput = scanner.nextLine();

        String gradeInput;
        System.out.println("Enter student grade");
        gradeInput = scanner.nextLine();
        gradesDB.put(nameInput,gradeInput);

    }
    public void removeStudent(){
        Scanner removeScanner = new Scanner(System.in);
        String removeInput;
        System.out.println("Enter student name to remove");
        removeInput = removeScanner.nextLine();
        gradesDB.remove(removeInput);
    }
    public void modifyStudent(){
        Scanner modifyScanner = new Scanner(System.in);
        String nameInput;
        System.out.println("Enter student name to modify");
        nameInput = modifyScanner.nextLine();

        String updateInput;
        System.out.println("Enter a grade to update");
        updateInput = modifyScanner.nextLine();
        gradesDB.replace(nameInput,updateInput);

    }
    public void printStudent(){
        for(String i: gradesDB.keySet()){System.out.println( i + " " + gradesDB.get(i) );}
    }
    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in);
        String input = "";
        P15_4 object = new P15_4();
        do{
            System.out.println("Select an option");
            System.out.println("(a) to add a student");
            System.out.println("(r) to remove a student");
            System.out.println("(m) to modify a grade");
            System.out.println("(p) print all grades");
            System.out.println("(q) to quit");
            input = myObj.nextLine();
            if(input.equals("a")){object.addStudent();}
            else if(input.equals("r")){object.removeStudent();}
            else if (input.equals("p")){object.printStudent();}
            else if (input.equals("m")){object.modifyStudent();}
            else{System.out.println("Invalid input. Either press an a or r or m or p or q");}

        }while(!input.equals("q"));

    }

}
