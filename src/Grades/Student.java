package Grades;

public class Student {
    String firstName;
    String lastName;
    int ID;
    String grade;
    Student(String fNameArg,String lNameArg,int IDArg, String gradeArg){
        firstName = fNameArg;
        lastName = lNameArg;
        ID = IDArg;
        grade = gradeArg;
    }
    int getID(){return ID;}
    void setGrade(String gradeArg){grade = gradeArg;}
}
