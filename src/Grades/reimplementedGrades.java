package Grades;
import java.util.Scanner;
import java.util.HashMap;

public class reimplementedGrades {
    //one hashmap key = id value = student object
    //one hashmap key = student object
    HashMap <Integer,Student> primaryDB = new HashMap<Integer, Student>();
    HashMap<Integer,Student> sortedDB= new HashMap<Integer,Student>();


    public void addNewStudent(){
        Scanner scanner = new Scanner(System.in);

        String firstNameInput;
        System.out.println("Enter student first name");
        firstNameInput = scanner.nextLine();

        String lastNameInput;
        System.out.println("Enter student last name");
        lastNameInput = scanner.nextLine();


        String IDInput;
        String loopExitLoop = "running";
        int id = 0;
        while(loopExitLoop == "running"){
            try{
                System.out.println("Enter student ID");
                IDInput = scanner.nextLine();
                Integer.parseInt(IDInput);
                loopExitLoop = "exiting loop";
            }
            catch(Exception e){System.out.println("input is not a number");}

        }


        String gradeInput;
        System.out.println("Enter student grade");
        gradeInput = scanner.nextLine();


        Student student = new Student(firstNameInput,lastNameInput,id,gradeInput);
        primaryDB.put(student.getID(),student);

    }
    public void removeStudent(){
        Scanner removeScanner = new Scanner(System.in);
        try{
            System.out.println("Enter the student ID ");
            int idInput;
            idInput = removeScanner.nextInt();
            primaryDB.remove(idInput);

        }




    }
    public void modifyStudent(){
        Scanner removeScanner = new Scanner(System.in);
        try {
            System.out.println("Enter the student ID ");
            int idInput;
            idInput = removeScanner.nextInt();
            Student studentOfID = primaryDB.get(idInput);

            System.out.println("Please enter the student's grade to update");
            String gradeInput;
            gradeInput = removeScanner.nextLine();
            studentOfID.setGrade(gradeInput);

        }
        catch(Exception e){System.out.println("there is an error");}
    }
    public void printStudent(){System.out.println("hello");}

    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in);
        String input = "";
        reimplementedGrades object = new reimplementedGrades();
        do{
            System.out.println("Select an option");
            System.out.println("(a) to add a student");
            System.out.println("(r) to remove a student");
            System.out.println("(m) to modify a grade");
            System.out.println("(p) print all grades");
            System.out.println("(q) to quit");
            input = myObj.nextLine();
            if(input.equals("a")){object.addNewStudent();}
            else if(input.equals("r")){object.removeStudent();}
            else if (input.equals("p")){object.printStudent();}
            else if (input.equals("m")){object.modifyStudent();}
            else{System.out.println("Invalid input. Either press an a or r or m or p or q");}

        }while(!input.equals("q"));
    }

}
